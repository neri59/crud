<?php
// session_start();
require_once "db-connexion.php";



$mail = $_POST['mail'];
$psw = $_POST['psw'];
if (isset($mail) && isset($psw)) {

    $mail = htmlspecialchars($mail);
    $psw = htmlspecialchars($psw);

    $check = $conn->prepare('SELECT email, password FROM user where email=?');
    $check->execute(array($mail));
    $data = $check->fetch();
    $row = $check->rowCount();

    if ($row == 1) {


        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {

            $psw = hash('sha256', $psw);
            if ($data['password'] === $psw) {
                header('Location:landing.php');
            } else header('Location:formulaire-connexion.php?login_err=password');
        } else header('Location:formulaire-connexion.php?login_err=email');
    } else header('Location:formulaire-connexion.php?login_err=empty');
} else {
    echo 'Je suis dans le ELSE';
    header('Location:formulaire-connexion.php');
}

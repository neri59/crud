<?php
include 'db-connexion.php';
include "index.php";
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>connexion</title>
    <link href="./css/style.css" rel="stylesheet">

</head>

<body>

    <body>



        <div class="container">


            <?php

            if (isset($_GET['reg_err'])) {
                $err = htmlspecialchars($_GET['reg_err']);

                switch ($err) {

                    case 'succes':
            ?>
                        <div class="alert alert-succes">
                            <strong>Succès</strong> inscription reussi
                        </div>
                    <?php
                        break;

                    case 'password':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> mot de passe incorect
                        </div>
                    <?php
                        break;

                    case 'email':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> email non valide
                        </div>
                    <?php
                        break;

                    case 'email_lenght':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> email trop long
                        </div>
                    <?php
                        break;

                    case 'already':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> compte deja existant
                        </div>
                    <?php
                        break;

                    case 'name':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> nom trop long
                        </div>
                    <?php
                        break;

                    case 'surname':
                    ?>
                        <div class="alert alert-danger">
                            <strong>Erreur</strong> prénom trop long
                        </div>
            <?php
                        break;
                }
            }

            ?>



            <form action="./inscription-traitement.php" method="post">

                <label for="name">Nom</label>
                <input type="text" id="name" name="name" required placeholder="Nom">
                <label for="lastname">Prénom</label>
                <input type="text" id="lastname" name="lastname" required placeholder="Prénom">
                <label for="mail">E-mail</label>
                <input type="email" id="mail" name="mail" required placeholder="E-mail">
                <label for="mdp">Mot de passe</label>
                <input type="password" id="mdp" name="mdp" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required placeholder="Mot de passe">
                <label for="confirmation">Confirmation du mot de passe</label>
                <input type="password" id="confirmation" name="confirmation" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required placeholder="Confirmez votre mot de passe">

                <button type="submit">
                    <p id="co">Je m'inscris</p>
                </button>
            </form>

            <div id="message">
                <h4>Le mot de passe doit contenir au minimim :</h4>
                <span id="letter" class="invalid">une lettre <b>minuscule</b> <br></span>
                <span id="capital" class="invalid">une lettre <b>majuscule</b> <br></span>
                <span id="number" class="invalid">un <b>chiffre</b> <br></span>
                <span id="length" class="invalid">Minimum <b>8 caractères</b> <br></span>
            </div>
        </div>



        <script>
            var myInput = document.getElementById("psw");
            var letter = document.getElementById("letter");
            var capital = document.getElementById("capital");
            var number = document.getElementById("number");
            var length = document.getElementById("length");

            // When the user clicks on the password field, show the message box
            myInput.onfocus = function() {
                document.getElementById("message").style.display = "block";
            }

            // When the user clicks outside of the password field, hide the message box
            myInput.onblur = function() {
                document.getElementById("message").style.display = "none";
            }

            // When the user starts to type something inside the password field
            myInput.onkeyup = function() {
                // Validate lowercase letters
                var lowerCaseLetters = /[a-z]/g;
                if (myInput.value.match(lowerCaseLetters)) {
                    letter.classList.remove("invalid");
                    letter.classList.add("valid");
                } else {
                    letter.classList.remove("valid");
                    letter.classList.add("invalid");
                }

                // Validate capital letters
                var upperCaseLetters = /[A-Z]/g;
                if (myInput.value.match(upperCaseLetters)) {
                    capital.classList.remove("invalid");
                    capital.classList.add("valid");
                } else {
                    capital.classList.remove("valid");
                    capital.classList.add("invalid");
                }

                // Validate numbers
                var numbers = /[0-9]/g;
                if (myInput.value.match(numbers)) {
                    number.classList.remove("invalid");
                    number.classList.add("valid");
                } else {
                    number.classList.remove("valid");
                    number.classList.add("invalid");
                }

                // Validate length
                if (myInput.value.length >= 8) {
                    length.classList.remove("invalid");
                    length.classList.add("valid");
                } else {
                    length.classList.remove("valid");
                    length.classList.add("invalid");
                }
            }
        </script>



        </div>

    </body>

</html>
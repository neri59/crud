<?php
include "db-connexion.php";
include "index.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div class=container>

        <?php
        if (isset($_GET['reg_err'])) {
            $err = htmlspecialchars($_GET['reg_err']);

            switch ($err) {

                case 'titre':
        ?>
                    <div class="alert alert-danger">
                        Le titre est trop long
                    </div>

                <?php
                    break;

                case 'artisteNom':
                ?>

                    <div class="alert alert-danger">
                        Le nom de l'artiste est trop long
                    </div>

                <?php
                    break;

                case 'artistePrenom':
                ?>

                    <div class="alert alert-danger">
                        Le prénom de l'artiste est trop long
                    </div>

                <?php
                    break;

                case 'label':
                ?>

                    <div class="alert alert-danger">
                        Le nom du label est trop long
                    </div>
        <?php
                    break;
            }
        }

        ?>



        <form action="./create-traitement.php" method="post">



            <label for="titre">Titre de la chanson</label>
            <input type="text" id="titre" name="titre" required placeholder="Nom de la chanson">

            <label for="artisteNom">Nom de l'artiste</label>
            <input type="text" id="artisteNom" name="artisteNom" required placeholder="Nom de l'artiste">

            <label for="artistePrenom">Prenom de l'artiste</label>
            <input type="text" id="artistePrenom" name="artistePrenom" required placeholder="Nom de l'artiste">


            <label for="label">Label</label>
            <input type="text" id="label" name="label" required placeholder="Label">

            <label for="annee">Année de sortie</label>
            <input type="text" id="annee" name="annee" required placeholder="Année de sortie">

            <button type="submit">
                <p id="co">Valider</p>
            </button>
        </form>


    </div>



</body>

</html>
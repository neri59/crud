<?php
include 'db-connexion.php';
include "index.php";

$id = $_GET['id'];
// print_r($id);

$sql = "SELECT * from disques inner join artistes on disques.idArtiste = artistes.idArtiste inner join labels on disques.idLabel = labels.idLabel where idDisque=$id;";
$req = $conn->prepare($sql);
$exeVerif = $req->execute();
// var_dump($exeVerif);
$row = $req->fetchObject();

?>
<div class="container-readInfo">
    <div class="row">
        <h5>Titre de la chanson : </h5> <?php echo  $row->titre; ?>
    </div>
    <div class="row">
        <h5>Nom de l'artiste : </h5> <?php echo  $row->nomArtiste; ?>
    </div>
    <div class="row">
        <h5>Prénom de l'artiste : </h5> <?php echo  $row->prenomArtiste; ?>
    </div>
    <div class="row">
        <h5>Label :</h5> <?php echo  $row->nomLabel; ?>
    </div>
    <div class="row">
        <h5>Année de sortie : </h5> <?php echo  $row->annee; ?>
    </div>
</div>
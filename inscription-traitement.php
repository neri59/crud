<?php
require_once "db-connexion.php";

// je stock les données récupérés dans le formulaire dans des variables
$mail = $_POST['mail'];
$mdp = $_POST['mdp'];
$confirmation = $_POST['confirmation'];
$name = $_POST['name'];
$lastname = $_POST['lastname'];


//je vérifie que les variable $_POST existe
if (isset($mail) && isset($mdp) && isset($confirmation) && isset($name) && isset($lastname)) {

    // Je convertie les chaines de caractères récupéré en HTML (pour éviter les insertions en BDD)
    $mail = htmlspecialchars($mail);
    $mdp = htmlspecialchars($mdp);
    $confirmation = htmlspecialchars($confirmation);
    $name = htmlspecialchars($name);
    $lastname = htmlspecialchars($lastname);

    //je verifie si les valeurs existe dans la base de donnée
    $check = $conn->prepare('SELECT email, password FROM user where email=?');
    $check->execute(array($mail));
    $row = $check->rowCount();

    // Si l'utilisateur n'existe pas en BDD
    if ($row == 0) {
        if (strlen($name) <= 50) {
            if (strlen($lastname) <= 50) {

                //je verifie que la longuer du login(email) soit bien inferieur a 100 caracteres
                if (strlen($mail) <= 100) {
                    if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                        if ($mdp == $confirmation) {

                            // On remplace $mdp par sa version haché (crypté)
                            $mdp = hash('sha256', $mdp);
                            // On crée la requête pour insérer le user en BDD
                            $insert = $conn->prepare('INSERT INTO user (name, surname, email, password) VALUES (?, ?, ?, ?)');
                            $insert->execute(array($name, $lastname, $mail, $mdp));
                            header('Location:inscription.php?reg_err=succes');
                        } else header('Location:inscription.php?reg_err=mot-de-passe');
                    } else header('Location:inscription.php?reg_err=email');
                } else header('Location:inscription.php?reg_err=email_length');
            } else header('Location:inscription.php?reg_err=lastname_length');
        } else header('Location:inscription.php?reg_err=name_length');
    } else
        // Si l'email est déjà présent dans la BDD
        header('Location:inscription.php?reg_err=already');
}

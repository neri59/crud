<?php
include "db-connexion.php";
include "index.php";
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="./css/style.css" rel="stylesheet">

</head>

<body>


    <div class="container">

        <?php

        if (isset($_GET['login_err'])) {
            $err = htmlspecialchars($_GET['login_err']);

            switch ($err) {

                case 'password':
        ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> mot de passe incorect
                    </div>
                <?php
                    break;

                case 'email':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> email incorect
                    </div>
                <?php
                    break;

                case 'empty':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> compte inexistant
                    </div>
        <?php
                    break;
            }
        }

        ?>


        <form action='connexion-traitement.php' method='post'>

            <label for="mail">E-mail</label>
            <input type="email" id="mail" name="mail" required placeholder="E-mail">

            <label for="psw">Mot de passe</label>
            <input type="password" id="psw" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required placeholder="Mot de passe">

            <button type="submit">
                <p id="co">Connexion</p>
            </button>
        </form>

        <div class="insc">
            <p>Vous n'avez pas encore de compte <a href="./inscription.php">cliquez ici</a>.</p>
        </div>

</body>

</html>
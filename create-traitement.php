<?php
require_once "db-connexion.php";



// je stock les données récupérés dans le formulaire dans des variables
$titre = $_POST['titre'];
$artisteNom = $_POST['artisteNom'];
$artistePrenom = $_POST['artistePrenom'];
$label = $_POST['label'];
$annee = $_POST['annee'];


//je vérifie que les variable $_POST existe
if (isset($titre) && isset($artisteNom) && isset($artistePrenom) && isset($label) && isset($annee)) {

    // Je convertie les chaines de caractères récupéré en HTML (pour éviter les insertions en BDD)
    $titre = htmlspecialchars($titre);
    $artisteNom = htmlspecialchars($artisteNom);
    $artistePrenom = htmlspecialchars($artistePrenom);
    $label = htmlspecialchars($label);
    $annee = htmlspecialchars($annee);






    //je verifie que le nom du label est inférieur a 50 caractères
    if (strlen($label) <= 50) {

        if (strlen($artistePrenom) <= 50) {
            if (strlen($artisteNom) <= 50) {
                if (strlen($titre) <= 50) {


                    //si les conditions sont remplies, je prépare ma requête, 
                    $insertLabel = $conn->prepare('INSERT INTO labels (nomLabel) VALUES (?)');
                    //puis je l'execute
                    $insertLabel->execute(array($label));
                    //je récupère l'id géneré 
                    $idLabel = $conn->lastInsertId();


                    //insertion table artistes
                    $insertArtiste = $conn->prepare('INSERT INTO artistes (nomArtiste, prenomArtiste) VALUES (?,?)');
                    $insertArtiste->execute(array($artisteNom, $artistePrenom));
                    $idArtiste = $conn->lastInsertId();

                    //insertion table disques
                    $insertTitre = $conn->prepare('INSERT INTO disques (titre, annee, idLabel, idArtiste ) VALUES (?,?,?,?)');
                    $insertTitre->execute(array($titre, $annee, $idLabel, $idArtiste));

                    //je redirige vers la page read.php
                    header('Location:read.php?reg_err=succes');
                } else header('Location:create.php?reg_err=titre-lenght');
            } else header('Location:create.php?reg_err=artisteNom-length');
        } else header('Location:create.php?reg_err=artistePrenom-length');
    } else header('Location:create.php?reg_err=label-length');
}

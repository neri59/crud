<?php
include 'db-connexion.php';
include "index.php";


$sql = "SELECT idDisque, titre, annee from disques order by titre asc;";
$req = $conn->prepare($sql);
$exeVerif = $req->execute();
// $results = $req->fetchObject();


// while ($results = $req->fetchobject()) {
// echo $results->titre . ' ' . $results->annee . '<br>';;
// }
// 
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" rel="stylesheet">
    <title>mon tableau</title>
</head>

<body>
    <h1>MON CRUD</h1>


    <table>

        <thead>
            <tr>
                <td class="radius">
                    <h2>Titre</h2>
                </td>
                <td class="">
                    <h2>Année</h2>
                </td>
                <td class="">modif</td>
                <td class="">supp</td>
                <td class="radius2">en savoir plus</td>
            </tr>
        </thead>
        <?php
        while ($results = $req->fetchObject()) { ?>
            <tbody>
                <tr>
                    <td> <?php echo $results->titre . ' ' . $results->idDisque; ?></td>
                    <td class="annee"><?php echo $results->annee; ?></td>
                    <td>
                        <div class="btn">Modif</div>
                    </td>
                    <td>
                        <form>
                            <input class='btn' type="submit" name="supp" value="Supprimer">
                        </form>
                        <!-- <div class="btn">supp</div> -->
                    </td>
                    <td>
                        <?php echo  '<a href="./readInfo.php? id=' . $results->idDisque . ' "> 
                        <div class="btn">+ d\'info... </div>
                        </a>' ?>
                    </td>

                </tr>
            </tbody>
        <?php
        }
        ?>

    </table>


</body>

</html>
<?php
$servername = "localhost";
$username = "root";
$password = "";

try {


  $conn = new PDO("mysql:host=$servername;dbname=disque", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  /*echo "Connecté avec succès";*/
} catch (PDOException $e) {
  echo "la connexion a échoué: " . $e->getMessage();
}
